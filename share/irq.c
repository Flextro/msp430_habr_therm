/*
 * main.c
 *
 * 	http://gpio.kaltpost.de/?page_id=726
 *
 */
//#include "..\share\nrf24l01.h"
#include  "msp430g2553.h"


/*********************************************************
 *
 *						IRQ VECTORS
 *
 *********************************************************/

/* IRQ VECTORS */
//USCIAB TX
//#pragma vector=USCIAB0TX_VECTOR
//__interrupt void USCI0TX_ISR(void)
//{
//
//}


//USCIAB RX
#pragma vector=USCIAB0RX_VECTOR
__interrupt void USCI0RX_ISR(void)
{
_NOP();
}


#pragma vector=ADC10_VECTOR
__interrupt void ADC10_ISR (void)
{
  __bic_SR_register_on_exit(CPUOFF);        // Clear CPUOFF bit from 0(SR)
}

#pragma vector=TIMER0_A0_VECTOR
__interrupt void ta0_isr(void)
{
  TACTL = 0;
  CCTL0 &= ~CCIE;
  LPM0_EXIT;
}

