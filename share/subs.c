/*
 * subs
 *
 * 	http://gpio.kaltpost.de/?page_id=726
 *
 */
//#include "nrf24l01.h"
#include  "msp430g2553.h"


void IO_init(void)
{
	/* LED0 */
	P1OUT &= ~LED0;
	P1DIR |= LED0;
}



void delay (unsigned long a)
{
	while(--a > 0);
	{
	_NOP();
	}
}

void delay_10us (unsigned long a)
{
	CCR0 = 10*a;
	TACTL = TASSEL_2 + MC_1;                  // SMCLK, upmode
	CCTL0 = CCIE;                             // CCR0 interrupt enabled
	__bis_SR_register(LPM0_bits + GIE);
}

void CSN_HIGH (void)
{
P1OUT |= CSN;
}

void CSN_LOW (void)
{
delay (CSN_TIME);
P1OUT &= ~CSN;
}

void CE_HIGH(void)
{
P1OUT |= CE;
}

void CE_LOW(void)
{
	P1OUT &= ~CE;
}

/*
 *
 * 		SPI && NRF
 *
 *
 */


unsigned char SPI_txByte(unsigned char byte)
{
	/*UCB0TXBUF = byte & 0xff;
	while (!(IFG2 & UCB0TXIFG));
	return UCB0RXBUF;*/
    UCA0TXBUF = byte & 0xff;
    while (!(IFG2 & UCA0TXIFG));
    return UCA0RXBUF;
}

void NRF_init(void)
{
	UCA0CTL1 = UCSWRST;
	/* CE */
	P1OUT &= ~CE;	//CE LOW
	P1DIR |= CE;
	/* CSN */
    P1OUT  |= CSN;	//CSN HIGH
    P1DIR  |= CSN;
  	P1SEL  |= MISO + MOSI + SCK;
  	P1SEL2 |= MISO + MOSI + SCK;

    // 3-pin, 8-bit SPI master
    UCA0CTL0 = UCCKPH + UCMSB + UCMST + UCSYNC;
	UCA0CTL1 |= UCSSEL_2;   // SMCLK
	UCA0BR0 = 0;
	UCA0BR1 = 0;

	UCA0CTL1 &= ~UCSWRST;
	//IE2 |= UCB0RXIE;
	//IE2 |= UCB0TXIE;

	//en_aa
    TXBuf[0]=0;	//disable AA
    NRF_WRreg(EN_AA, 1);
    //en_rx_addr
    TXBuf[0]=EN_RXADDR_DEFAULT_VAL;
    NRF_WRreg(EN_RXADDR, 1);
    //setup_aw
    TXBuf[0]=SETUP_AW_DEFAULT_VAL;
    NRF_WRreg(SETUP_AW, 1);
    //setup_retr
    TXBuf[0]=SETUP_RETR_DEFAULT_VAL;
    NRF_WRreg(SETUP_RETR, 1);
    //rf_ch
    TXBuf[0]=RF_CH_DEFAULT_VAL;
    NRF_WRreg(RF_CH, 1);
    //rf_setup
    TXBuf[0]=RF_SETUP_DEFAULT_VAL;
    NRF_WRreg(RF_SETUP, 1);
    //rx_addr_p0
    TXBuf[0] = RX_ADDR_P0_B0_DEFAULT_VAL;
    TXBuf[1] = RX_ADDR_P0_B1_DEFAULT_VAL;
    TXBuf[2] = RX_ADDR_P0_B2_DEFAULT_VAL;
    TXBuf[3] = RX_ADDR_P0_B3_DEFAULT_VAL;
    TXBuf[4] = RX_ADDR_P0_B4_DEFAULT_VAL;
    NRF_WRreg(RX_ADDR_P0, 5);
    //rx_addr_p1
    TXBuf[0] = RX_ADDR_P1_B0_DEFAULT_VAL;
    TXBuf[1] = RX_ADDR_P1_B1_DEFAULT_VAL;
    TXBuf[2] = RX_ADDR_P1_B2_DEFAULT_VAL;
    TXBuf[3] = RX_ADDR_P1_B3_DEFAULT_VAL;
    TXBuf[4] = RX_ADDR_P1_B4_DEFAULT_VAL;
    NRF_WRreg(RX_ADDR_P1, 5);
    //rx_addr_p2
    TXBuf[0] = RX_ADDR_P2_DEFAULT_VAL;
    NRF_WRreg(RX_ADDR_P2, 1);
    //rx_addr_p3
    TXBuf[0] = RX_ADDR_P3_DEFAULT_VAL;
    NRF_WRreg(RX_ADDR_P3, 1);
    //rx_addr_p4
    TXBuf[0] = RX_ADDR_P4_DEFAULT_VAL;
    NRF_WRreg(RX_ADDR_P4, 1);
    //rx_addr_p5
    TXBuf[0] = RX_ADDR_P5_DEFAULT_VAL;
    NRF_WRreg(RX_ADDR_P5, 1);
    //tx_addr
    TXBuf[0] = TX_ADDR_B0_DEFAULT_VAL;
    TXBuf[1] = TX_ADDR_B1_DEFAULT_VAL;
    TXBuf[2] = TX_ADDR_B2_DEFAULT_VAL;
    TXBuf[3] = TX_ADDR_B3_DEFAULT_VAL;
    TXBuf[4] = TX_ADDR_B4_DEFAULT_VAL;
    NRF_WRreg(TX_ADDR, 5);

    //rx_payload_width_p0
    TXBuf[0] = payloadWidth;
    NRF_WRreg(RX_PW_P0, 1);

    //rx_payload_width_p1
    TXBuf[0] = RX_PW_P1_DEFAULT_VAL;
    NRF_WRreg(RX_PW_P1, 1);

    //rx_payload_width_p2
    TXBuf[0] = RX_PW_P2_DEFAULT_VAL;
    NRF_WRreg(RX_PW_P2, 1);

    //rx_payload_width_p3
    TXBuf[0] = RX_PW_P3_DEFAULT_VAL;
    NRF_WRreg(RX_PW_P3, 1);

    //rx_payload_width_p4
    TXBuf[0] = RX_PW_P4_DEFAULT_VAL;
    NRF_WRreg(RX_PW_P4, 1);

    //rx_payload_width_p5
    TXBuf[0] = RX_PW_P5_DEFAULT_VAL;
    NRF_WRreg(RX_PW_P5, 1);

    //power up
    TXBuf[0]= CONFIG_DEFAULT_VAL | CONFIG_PWR_UP | CONFIG_EN_CRC | CONFIG_CRCO;
    if(defaultRX){TXBuf[0]|= CONFIG_PRIM_RX;}	// default rx or tx
    NRF_WRreg(CONFIG, 1);
    if (defaultRX)
    {
    	CE_HIGH();
    }
    else
    {
    	CE_LOW();
    }
    delay_10us(3);

}

void NRF_WRreg(unsigned char addr, unsigned char len)
{
	unsigned char ind=0;
	unsigned char res;
	CSN_LOW();
	res=SPI_txByte(W_REGISTER|(W_REGISTER_DATA&addr));
//	putChar(0x0a);
//	putChar(0x0d);
//	putChar('W');
//	putChar(':');
//	hex2uart(addr>>4);
//	hex2uart(addr);
//	putChar('-');
	while(ind<len)
	{
		res=SPI_txByte(TXBuf[ind]);
//		hex2uart(TXBuf[ind]>>4);
//		hex2uart(TXBuf[ind]);
		ind++;
	}
	CSN_HIGH();
}


void NRF_RDreg(unsigned char addr, unsigned char len)
{
	unsigned char ind=0;
	CSN_LOW();
	status=SPI_txByte(R_REGISTER|(R_REGISTER_DATA&addr));
//	putChar(0x0a);
//	putChar(0x0d);
//	putChar('R');
//	hex2uart(addr>>4);
//	hex2uart(addr);
//	putChar(':');
	while(ind<len)
	{
		RXBuf[ind]=SPI_txByte(NOP);
//		hex2uart(RXBuf[ind]>>4);
//		hex2uart(RXBuf[ind]);
		ind++;
	}
	CSN_HIGH();
}

unsigned char NRF_rdOneReg(unsigned char addr)
{
	unsigned char oneReg;
	CSN_LOW();
	status=SPI_txByte(R_REGISTER|(R_REGISTER_DATA&addr));
	oneReg=SPI_txByte(NOP);
	CSN_HIGH();
	return oneReg;
}


void NRF_cmd(unsigned char cmd, unsigned char len)
{
	unsigned char ind=0;
	unsigned char res;
	CSN_LOW();
	res=SPI_txByte(cmd);
	while(ind<len)
	{
		RXBuf[ind]=SPI_txByte(TXBuf[ind]);
		ind++;
	}
	CSN_HIGH();
}

void NRF_transmit(void)
{
	CE_HIGH();
	delay_10us(3);
	CE_LOW();
}

void NFR_readRX(void)
{
	CE_LOW();
	NRF_cmd(R_RX_PAYLOAD,payloadWidth);
	CE_HIGH();
}

void NRF_irq_clear_all(void)
{
	TXBuf[0]=STATUS_RX_DR | STATUS_TX_DS | STATUS_MAX_RT;
	NRF_WRreg(STATUS,1);
}

unsigned char NRF_status(void)
{
	CSN_LOW();
	status=SPI_txByte(R_REGISTER|CONFIG);
	CSN_HIGH();
	return status;
}


void NRF_flush_rx(void)
{
	CSN_LOW();
	status=SPI_txByte(FLUSH_RX);
	CSN_HIGH();

}

void NRF_flush_tx(void)
{
	CSN_LOW();
	status=SPI_txByte(FLUSH_TX);
	CSN_HIGH();

}
void NRF_2RX(void)
{
	TXBuf[0]=NRF_rdOneReg(CONFIG);
	TXBuf[0] |= CONFIG_PRIM_RX;
	NRF_WRreg(CONFIG,1);
	CE_HIGH();
}

void NRF_2TX (void)
{
	TXBuf[0]=NRF_rdOneReg(CONFIG);
	TXBuf[0] &= ~CONFIG_PRIM_RX;
	NRF_WRreg(CONFIG,1);
	CE_LOW();
}

void NRF_up(void)
{
	TXBuf[0]=NRF_rdOneReg(CONFIG);
	TXBuf[0] |= CONFIG_PWR_UP;
	NRF_WRreg(CONFIG,1);
	CE_LOW();
}

void NRF_down(void)
{
	NRF_irq_clear_all();
	TXBuf[0]=NRF_rdOneReg(CONFIG);
	TXBuf[0] &= ~CONFIG_PWR_UP;
	NRF_WRreg(CONFIG,1);
	CE_LOW();
}






/*
 *
 * UART
 *
 */
/*void UART_init(void)
{
	UCA0CTL1 = UCSWRST;
	BCSCTL1 = CALBC1_1MHZ;                    // Set DCO
	DCOCTL = CALDCO_1MHZ;
	P1SEL |= RXD + TXD;                     // P1.1 = RXD, P1.2=TXD
	P1SEL2 |= RXD + TXD;
	UCA0CTL1 |= UCSSEL_2;                     // SMCLK
	UCA0BR0 = 8;                              // 1MHz 115200
	UCA0BR1 = 0;                              // 1MHz 115200
	UCA0MCTL = UCBRS2 + UCBRS0;               // Modulation UCBRSx = 5
	UCA0CTL1 &= ~UCSWRST;
	IE2 |= UCA0RXIE;
}*/


/* ���� � uart */
/*void putChar(unsigned char byte)
{
	while(!(IFG2 & UCA0TXIFG));
	UCA0TXBUF = byte & 0xFF;
	while(!(IFG2 & UCA0TXIFG));
}*/

/* �������� � uart HEX-�������� ������ */
/*static void hex2uart(unsigned char n) {
	static const char hex[16] = { '0', '1', '2', '3', '4', '5', '6', '7', '8',
			'9', 'A', 'B', 'C', 'D', 'E', 'F' };
	while (!(IFG2&UCA0TXIFG));
	UCA0TXBUF=(hex[n & 15]);
}*/

