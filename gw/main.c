/*
 * main.c
 *
 * 	http://gpio.kaltpost.de/?page_id=726
 *
 */


#define defaultRX		1
#define payloadWidth	8


unsigned char i;
unsigned char tmp;
unsigned int tmp1;
#include "..\share\nrf24l01.h"
#include  "msp430g2553.h"
#include "..\share\subs.c"
#include "..\share\irq.c"

void main(void) {
	/* inits */
	WDTCTL = WDTPW + WDTHOLD;                 // Stop WDT
	IO_init();
	UART_init();
	NRF_init();
	NRF_irq_clear_all();
	//NRF_flush_tx();
	//TXBuf[0]=STATUS_DEFAULT_VAL;
	//NRF_WRreg(STATUS,1);
	while(1)
	{
		P1OUT ^= LED0;
		putChar(0x0a);
		putChar(0x0d);
		/*delay(1000);
		//NRF_cmd(FLUSH_TX,1);
		i++;
		TXBuf[0]=i;
		TXBuf[1]=~i;
	    NRF_cmd(W_TX_PAYLOAD, payloadWidth);
	    NRF_transmit();
		putChar(0x0a);
		putChar(0x0d);
		tmp=NRF_status();
		hex2uart(tmp>>4);
		hex2uart(tmp);
		putChar('|');
		hex2uart(i>>4);
		hex2uart(i);
		while(!(NRF_status() & STATUS_TX_DS));	//wait tx
	    NRF_irq_clear_all();
	    // wait answer
	    NRF_2RX();
	    tmp1=0;
	    putChar(':');*/
		tmp1=0;
	    while(tmp1<10000)
	    {
	    	tmp1++;
	    	if(NRF_status() & STATUS_RX_DR)
	    	{
	    		//receiwed
	    		NFR_readRX();
	    		break;
	    	}
	    	delay_10us(10);
	    }
	    if(tmp1==10000)
	    {
			tmp=NRF_status();
			hex2uart(tmp>>4);
			hex2uart(tmp);
			putChar('|');
	    	putChar('l');
	    	putChar('o');
	    	putChar('s');
	    	putChar('t');
	    }else{
	    	hex2uart(RXBuf[0]>>4);
	    	hex2uart(RXBuf[0]);
	    	hex2uart(RXBuf[1]>>4);
	    	hex2uart(RXBuf[1]);
	    	hex2uart(RXBuf[2]>>4);
	    	hex2uart(RXBuf[2]);
	    	hex2uart(RXBuf[3]>>4);
	    	hex2uart(RXBuf[3]);
	    	hex2uart(RXBuf[4]>>4);
	    	hex2uart(RXBuf[4]);
	    	hex2uart(RXBuf[5]>>4);
	    	hex2uart(RXBuf[5]);
	    	hex2uart(RXBuf[6]>>4);
	    	hex2uart(RXBuf[6]);
	    	hex2uart(RXBuf[7]>>4);
	    	hex2uart(RXBuf[7]);
	    }
	    NRF_irq_clear_all();
	    delay_10us(15);
//	    NRF_2TX();
		/*
		CSN_LOW();
		tmp=SPI_txByte(R_REGISTER|(R_REGISTER_DATA&TX_ADDR));
		putChar(0x0a);
		putChar(0x0d);
		hex2uart(tmp>>4);
		hex2uart(tmp);
		putChar(':');
		while(i<5)
		{
			tmp=SPI_txByte(NOP);
			hex2uart(tmp>>4);
			hex2uart(tmp);
			i++;
		}
		CSN_HIGH();
		*/
	}
}
