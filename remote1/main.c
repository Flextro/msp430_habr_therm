/*
 * main.c
 */


#define defaultRX			0
#define payloadWidth		8
#define myIdHigh			0x11
#define myIdLow				0x01
#define opCode1				0X01

#define status_OK			0X00
#define status_BattLow		0X01

unsigned char i;
unsigned char tmp;
unsigned char seq;


#include "..\share\nrf24l01.h"
#include  "msp430g2553.h"

volatile unsigned int  	FLAGS;
volatile unsigned char 	i2cBUF;
volatile unsigned int 	i2cWORD, deepSleepCnt, deepSleepTimer;

#define 	RXed			BIT0	// ��� ����� ����� i2c
#define 	i2cTX			BIT1	// ���� �������� i2c

/*	i2c	defines	*/
#define	TPWR		BIT0
#define	TPWRPortOUT	P2OUT
#define	TPWRPortDIR	P2DIR
#define	i2cPortOUT	P1OUT
#define	i2cPortDIR	P1DIR
#define	i2cPortSEL	P1SEL
#define	i2cPortSEL2	P1SEL2
#define	i2cPortREN	P1REN
#define	i2cSDA		BIT7
#define	i2cSCK		BIT6

#define	Ti2cAddr						0x48 // shifted left. DS1624 and LM75A has same address
#define DS1621_CMD_ACCESSCONFIG        	0xAC
#define DS1621_CMD_INITIATECONVERT     	0xEE
#define DS1621_CMD_ACCESSTEMPERATURE   	0xAA
#define DS1621_CMD_READ_COUNTER_REMAIN 	0xA8
#define CONVERSION_DONE 				BIT7

// 1 = 4 sec
//#define deepSleepTime				30
#define deepSleepTime               2
//TODO: redefine for release
#define deepSleepTimeLowBatt		150
#define LowBattADC					0x1c0



void termInit(void);
void termDown(void);
void i2cReadByte(unsigned char addr);
void i2cReadWord(unsigned char addr);
void i2cSendByte(unsigned char addr,unsigned char byte);
void i2cSendCmd(unsigned char cmd);
void deepSleep(unsigned char dsCnt);
void sleepDelay(unsigned int cnt);

void main(void) {
	/* inits */
	WDTCTL = WDTPW + WDTHOLD;                 // Stop WDT
	seq = 0;
	while(1)
	{
		//P1OUT |=LED0;
	    seq++;
		IO_init();
		//UART_init();
		termInit();


/*		i2cReadByte(DS1621_CMD_ACCESSCONFIG);
		while(!(i2cBUF & CONVERSION_DONE))
		{
			sleepDelay(1);
			i2cReadByte(DS1621_CMD_ACCESSCONFIG);
		}

*/ //not needed to LM75A

//		hex2uart(i2cBUF>>4);
//		hex2uart(i2cBUF);
//		putChar(':');

		//read LM75A temp
		//i2cSendCmd("0x00"); // temp reg addr
		//i2cReadWord(DS1621_CMD_ACCESSTEMPERATURE);
		i2cReadWord(0x00); // read 2 bytes by pointer 0x00? SrW 0x00 SrR 1 2 Sp
		termDown();



		//P1OUT |=LED0;
		//get voltage
		ADC10CTL1 = INCH_11;                      // AVcc/2
		ADC10CTL0 = SREF_1 + ADC10SHT_2 + REFON + REF2_5V + ADC10ON + ADC10IE;
	    ADC10CTL0 |= ENC + ADC10SC;             // Sampling and conversion start
	    __bis_SR_register(CPUOFF + GIE);        // LPM0, ADC10_ISR will force exit
	    ADC10CTL0=0x0;
//*/

//*
//termInit();
//termDown();
	    //NRF_PowerUP();
		NRF_init();
		TXBuf[0]=myIdHigh;
		TXBuf[1]=seq;
		TXBuf[2]=opCode1;
		if (ADC10MEM>LowBattADC)
		{
			TXBuf[3]=status_OK;
		}else{
			TXBuf[3]=status_BattLow;
		}
		TXBuf[4]=(i2cWORD>>8) & 0Xff;
		TXBuf[5]=i2cWORD & 0Xff;
		TXBuf[6]=(ADC10MEM>>8) & 0Xff;
		TXBuf[7]=ADC10MEM & 0Xff;
		NRF_cmd(W_TX_PAYLOAD, payloadWidth);
	    NRF_transmit();

		while(!(NRF_status() & STATUS_TX_DS))	//wait tx
		{
			//tmp=NRF_status();
			//putChar(0x0a);
			//putChar(0x0d);
			//hex2uart(tmp>>4);
			//hex2uart(tmp);
		}
		NRF_down();

		//P1OUT &=~LED0;
		//putChar(0x0a);
		//putChar(0x0d);
		//tmp=NRF_status();
		//hex2uart(tmp>>4);
		//hex2uart(tmp);
		//putChar('|');

		//NRF_PowerDOWN();
//*/

		/*
		hex2uart(i2cWORD>>12);
		hex2uart(i2cWORD>>8);
		hex2uart(i2cWORD>>4);
		hex2uart(i2cWORD);
	    putChar(':');
		hex2uart(ADC10MEM>>12);
		hex2uart(ADC10MEM>>8);
		hex2uart(ADC10MEM>>4);
		hex2uart(ADC10MEM);
		*/

		//check battery voltage
		if(ADC10MEM>LowBattADC)
		{
			deepSleep(deepSleepTime);
		}else{
			deepSleep(deepSleepTimeLowBatt);
		}

	}
}

/*
 * i2c:
 * mosi(p1.7) = SDA
 * miso(p1.6) = SCK
 * */
#include "..\share\irq.c"

#pragma vector = USCIAB0TX_VECTOR
__interrupt void USCIAB0TX_ISR(void)
{
	if (FLAGS & i2cTX)
	{
		UCB0TXBUF=i2cBUF;
		__bic_SR_register_on_exit(CPUOFF);

	}else{
		if (!(FLAGS & RXed))
		{
			i2cBUF=UCB0RXBUF;
			FLAGS |= RXed;
			UCB0CTL1 |= UCTXSTP;
			IFG2 &= ~UCB0TXIFG;
			__bic_SR_register_on_exit(CPUOFF);
		}else{
			//i2cBUF=UCB0RXBUF;
			i2cWORD|=UCB0RXBUF;
			IFG2 &= ~UCB0RXIFG;
		}
	}
}


#pragma vector=WDT_VECTOR
__interrupt void watchdog_timer (void)
{
	//P1OUT ^=LED0;
	if(++deepSleepCnt > deepSleepTimer)
	{
		_BIC_SR_IRQ(LPM3_bits);                   // Clear LPM3 bits from 0(SR)
	}
}


void termInit(void)
{
	//init i2c
    P1OUT  |= CSN;	//CSN HIGH
    P1DIR  |= CSN;

    i2cPortSEL |= i2cSDA + i2cSCK;                     	//Set I2C pins
	i2cPortSEL2 |= i2cSDA + i2cSCK;
	TPWRPortOUT |= TPWR;
	TPWRPortDIR |= TPWR;
	//P1OUT |= LED0;



	//i2cSendByte(DS1621_CMD_ACCESSCONFIG,0x01);			//one shot conversion
	//i2cSendCmd(DS1621_CMD_INITIATECONVERT);
}

void termDown(void)
{
	TPWRPortOUT &= ~TPWR;
	TPWRPortDIR |= TPWR;
	i2cPortSEL &= ~(i2cSDA + i2cSCK);                     	//reset I2C pins
	i2cPortSEL2 &= ~(i2cSDA + i2cSCK);
	//P1OUT &= ~LED0;
}

// ������ ���� �� i2c �� ������ addr
static void i2cReadByte(unsigned char addr)
{
	while (UCB0CTL1 & UCTXSTP);
	UCB0CTL1 = UCSWRST;                     		//Enable SW reset
	UCB0CTL0 = UCMST + UCMODE_3 + UCSYNC;         	//I2C Master, synchronous mode
	UCB0CTL1 = UCSSEL_2 + UCSWRST;               	//Use SMCLK, keep SW reset
	UCB0BR0 = 12;                           		//fSCL = SMCLK/12 = ~100kHz
	UCB0BR1 = 0;
	UCB0I2CSA = Ti2cAddr;                     		//Slave Address
	UCB0CTL1 &= ~UCSWRST;                     		//Clear SW reset, resume operation
	IE2 |= UCB0TXIE;                        		//Enable TX interrupt
	IE2 &= ~UCB0RXIE;                        		//Enable RX interrupt
	FLAGS |=i2cTX;
	i2cBUF=addr;
	//P1OUT|=LED0;
	UCB0CTL1 |= UCTR + UCTXSTT;						//tx+start
	__bis_SR_register(CPUOFF + GIE);				// Enter LPM0 w/ interrupts
	FLAGS &= ~i2cTX;
	IE2 &= ~UCB0TXIE;
	UCB0CTL1 &=~UCTR;								//rx
	UCB0CTL1 |=	UCTXSTT;
	FLAGS &= ~RXed;
	IE2 |= UCB0RXIE;                        		//Enable RX interrupt
	__bis_SR_register(CPUOFF + GIE);            	// Enter LPM0 w/ interrupts
	UCB0CTL1 |= UCTXSTP;
	while (!(FLAGS & RXed));
	while (UCB0CTL1 & UCTXSTP);            			// Ensure stop condition got sent
	//P1OUT&=~LED0;
}

void i2cReadWord(unsigned char addr)
{
	while (UCB0CTL1 & UCTXSTP);
	UCB0CTL1 = UCSWRST;                     		//Enable SW reset
	UCB0CTL0 = UCMST + UCMODE_3 + UCSYNC;         	//I2C Master, synchronous mode
	UCB0CTL1 = UCSSEL_2 + UCSWRST;               	//Use SMCLK, keep SW reset
	UCB0BR0 = 12;                           		//fSCL = SMCLK/12 = ~100kHz
	UCB0BR1 = 0;
	UCB0I2CSA = Ti2cAddr;                     		//Slave Address
	UCB0CTL1 &= ~UCSWRST;                     		//Clear SW reset, resume operation
	IE2 |= UCB0TXIE;                        		//Enable TX interrupt
	IE2 &= ~UCB0RXIE;                        		//Enable RX interrupt
	FLAGS |=i2cTX;
	i2cBUF=addr;
	//P1OUT|=LED0;
	UCB0CTL1 |= UCTR + UCTXSTT;						//tx+start
	__bis_SR_register(CPUOFF + GIE);				// Enter LPM0 w/ interrupts
	FLAGS &= ~i2cTX;
	IE2 &= ~UCB0TXIE;
	UCB0CTL1 &=~UCTR;								//rx
	UCB0CTL1 |=	UCTXSTT;
	FLAGS &= ~RXed;
	IE2 |= UCB0RXIE;                        		//Enable RX interrupt
	__bis_SR_register(CPUOFF + GIE);            	//Enter LPM0 w/ interrupts
	i2cWORD=i2cBUF<<8;
	UCB0CTL1 |= UCTXSTP;							//send stop
	while (!(FLAGS & RXed));
	while (UCB0CTL1 & UCTXSTP);            			//Ensure stop condition got sent
	//i2cWORD|=i2cBUF;                                //maybe???
	//P1OUT&=~LED0;

}
// ����� ���� � i2c �� ������ addr
void i2cSendByte(unsigned char addr, unsigned char byte)
{
	while (UCB0CTL1 & UCTXSTP);	// wait stop condition is sent
	UCB0CTL1 = UCSWRST;                     //Enable SW reset
	UCB0CTL0 = UCMST + UCMODE_3 + UCSYNC;         //I2C Master, synchronous mode
	UCB0CTL1 = UCSSEL_2 + UCSWRST;               //Use SMCLK, keep SW reset
	UCB0BR0 = 12;                           //fSCL = SMCLK/12 = ~100kHz
	UCB0BR1 = 0;
	UCB0I2CSA = Ti2cAddr;                     	//Slave Address  (Thermometer address HERE)
	UCB0CTL1 &= ~UCSWRST;                   //Clear SW reset, resume operation
	IE2 |= UCB0TXIE;                        //Enable TX interrupt
 	IE2 &= ~UCB0RXIE;                       //Enable RX interrupt
	FLAGS |=i2cTX;
	i2cBUF=addr;
	UCB0CTL1 |= UCTR + UCTXSTT; //tx+start
	__bis_SR_register(CPUOFF + GIE);            // Enter LPM0 w/ interrupts
	UCB0TXBUF=byte;
 	__bis_SR_register(CPUOFF + GIE);
	UCB0CTL1 |= UCTXSTP;               // I2C stop condition
	while (UCB0CTL1 & UCTXSTP);            // Ensure stop condition got sent
}

// ����� ���� � i2c �� ������ addr
void i2cSendCmd(unsigned char cmd)
{
	while (UCB0CTL1 & UCTXSTP);				// wait stop condition is sent
	UCB0CTL1 = UCSWRST;                    //Enable SW reset
	UCB0CTL0 = UCMST + UCMODE_3 + UCSYNC;   //I2C Master, synchronous mode
	UCB0CTL1 = UCSSEL_2 + UCSWRST;          //Use SMCLK, keep SW reset
	UCB0BR0 = 12;                           //fSCL = SMCLK/12 = ~100kHz
	UCB0BR1 = 0;
	UCB0I2CSA = Ti2cAddr;                   //Slave Address  (Thermometer address HERE)
	UCB0CTL1 &= ~UCSWRST;                   //Clear SW reset, resume operation
	IE2 |= UCB0TXIE;                        //Enable TX interrupt
 	IE2 &= ~UCB0RXIE;                       //Enable RX interrupt
	FLAGS |=i2cTX;
	i2cBUF=cmd;
	UCB0CTL1 |= UCTR + UCTXSTT; 			//tx+start
	__bis_SR_register(CPUOFF + GIE);        // Enter LPM0 w/ interrupts
	UCB0CTL1 |= UCTXSTP;               		// I2C stop condition
	while (UCB0CTL1 & UCTXSTP);            	// Ensure stop condition got sent
}

void deepSleep(unsigned char dsCnt)
{
	ADC10CTL0=0;
	BCSCTL1 |= DIVA_2;                        // ACLK/4
	WDTCTL = WDT_ADLY_1000;                   // WDT 1s/4 interval timer
	P1DIR = 0xFF;                             // All P1.x outputs
	P1OUT = 0;                                // All P1.x reset
	P2DIR = 0xFF;                             // All P2.x outputs
	P2OUT = 0;                                // All P2.x reset
	deepSleepCnt=0;
	deepSleepTimer=dsCnt;
	IE1 = WDTIE;                             // Enable WDT interrupt
	_BIS_SR(LPM3_bits + GIE);               // Enter LPM3
	WDTCTL=0;
}

void sleepDelay(unsigned int cnt)
{
	BCSCTL1 |= DIVA_2;                      // ACLK/4
	WDTCTL = WDT_ADLY_16;                   //
	deepSleepCnt=0;
	deepSleepTimer=cnt;
	IE1 |= WDTIE;                           // Enable WDT interrupt
	_BIS_SR(LPM3_bits + GIE);               // Enter LPM3
	BCSCTL1 &=~DIVA_2;
	IE1 &= ~WDTIE;                          // Enable WDT interrupt
	WDTCTL = WDTPW + WDTHOLD;               // Stop WDT
}

#include "..\share\subs.c"

